﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task7 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int[] array = new int[10];
        string moreThanZero = "";
        for (int i = 0; i < 10; i++)
        {
            array[i] = Random.Range(-7, 9);
            if (array[i] > 0)
            {
                moreThanZero += i + " ";
            }
        }
        Debug.Log(moreThanZero);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
