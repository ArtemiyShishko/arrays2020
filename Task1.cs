﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task1 : MonoBehaviour
{
    // Start is called before the first frame update
    public Text text1, text2;
    void Start()
    {
        
        int[] array = new int[10];
        for (int i =0;i<10;i++)
        {
            array[i] = Random.Range(0, 4);
            text1.text += array[i];
            if (array[i] != 2)
                text2.text += array[i];
            else
                text2.text += 0;
        }
    }
}
