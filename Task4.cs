﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task4 : MonoBehaviour
{
    // Start is called before the first frame update
    public Text text4;
    void Start()
    {
        int sum =0, comp=1;
        int[] array = new int[10];
        for (int i = 0; i < 10; i++)
        {
            array[i] = Random.Range(-7, 9);
            text4.text += array[i];
            sum = sum + array[i];
            comp = comp * array[i];
        }
        text4.text += " " + sum;
        text4.text += " " + comp;
    }
}
