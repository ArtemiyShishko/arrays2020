﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task6 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int[] array = new int[20];
        int[] lessThanZero = new int[20];
        int[] moreThanZero = new int[20];
        string more = "", less = "";
        int a = 0, b = 0;
        for (int i = 0; i < 20; i++)
        {
            array[i] = Random.Range(-7, 9);
            if (array[i] > 0)
            {
                moreThanZero[a] = array[i];
                a++;
            }
            if (array[i] < 0)
            {
                lessThanZero[b] = array[i];
                b++;
            }
        }
        for (int i = 0; i < 20; i++)
        {
            if (moreThanZero[i]!=0)
                more += (moreThanZero[i] + " ");
        }
        for (int i = 0; i < 20; i++)
        {
            if (lessThanZero[i] != 0)
                less += (lessThanZero[i] + " ");
        }
        Debug.Log(more);
        Debug.Log(less);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
