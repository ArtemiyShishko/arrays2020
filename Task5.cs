﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task5 : MonoBehaviour
{
    public Text text4, text5;
    public InputField iField, iField2;
    int num, place;
    int[] array = new int[10];
    void Start()
    {
        for (int i = 0; i < 9; i++)
        {
            array[i] = Random.Range(0, 39);
            text4.text += array[i] + " ";
        }
    }
    void Update()
    {
        
        
    }
    public void ReadButton()
    {
        num = int.Parse(iField.text);
        place = int.Parse(iField2.text);
        for (int i = 9; i > place; i--)
        {
            array[i] = array[i - 1];
        }
        array[place] = num;
        for (int i = 0; i < 10; i++)
        {
            text5.text += array[i] + " ";
        }
    }
}

