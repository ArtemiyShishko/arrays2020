﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task3 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int max = 0;
        int[] array = new int[10];
        for (int i = 0; i < 10; i++)
        {
            array[i] = Random.Range(-7, 9);
            if (array[i] < 0 && array[i] < max)
                max = array[i];
        }
        if (max == 0)
            Debug.Log("Отрицательных элементов нет");
        else
            Debug.Log("Наибольший отрицательный элемент " + max);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
